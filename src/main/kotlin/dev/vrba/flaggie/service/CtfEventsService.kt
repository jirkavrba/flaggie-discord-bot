package dev.vrba.flaggie.service

import arrow.core.*
import dev.vrba.flaggie.discord.DiscordConfiguration
import dev.vrba.flaggie.entity.CtfEvent
import dev.vrba.flaggie.repository.CtfEventsRepository
import dev.vrba.flaggie.shared.CzechDateTimeFormatter
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Category
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Role
import org.hibernate.annotations.NotFound
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.awt.Color
import java.time.LocalDateTime
import java.util.*

@Service
class CtfEventsService(private val repository: CtfEventsRepository, private val config: DiscordConfiguration, client: JDA) {

    private val eventsChannel = client.getTextChannelById(config.eventsChannel)
            ?: throw IllegalStateException("Cannot find events channel")

    private val guild = eventsChannel.guild

    data class RawEventData(
            val name: String,
            val website: String,
            val start: String,
            val end: String,
            val format: String? = null,
            val access: String? = null
    )

    data class ValidationError(val reason: String)

    data class NotFoundError(val available: Collection<UUID>)

    fun create(data: RawEventData): Either<List<ValidationError>, CtfEvent> =
            validate(data)
                    .map {
                        val role = createEventRole(it)
                        val message = createEventMessage(it, role.color)
                        val category = createEventCategory(it, role, message)

                        message.addReaction(config.eventsReaction).queue()

                        category.createTextChannel(it.name).queue()
                        category.createVoiceChannel(it.name).queue()

                        repository.save(CtfEvent(
                                name = it.name,
                                url = it.website, // TODO: Rename this to url?
                                start = parse(it.start),
                                end = parse(it.end),
                                access = it.access,
                                format = it.format,
                                messageId = message.id,
                                roleId = role.id,
                                categoryId = category.id,
                        ))
                    }

    fun archive(id: String): Either<NotFoundError, CtfEvent> =
        safelyParseUUID(id)
                .flatMap { repository.findByIdOrNull(it).toOption() }
                .map {
                    repository.delete(it)
                    guild.getRoleById(it.roleId)?.delete()?.queue()
                    eventsChannel.clearReactionsById(it.messageId).queue()

                    it
                }
                .toEither { NotFoundError(repository.findAll().map { it.id }) }

    fun delete(id: String): Either<NotFoundError, CtfEvent> =
            archive(id).map {
                val category = guild.getCategoryById(it.categoryId)

                category?.channels?.map { channel -> channel.delete().queue() }
                category?.delete()?.queue()

                it
            }

    private fun createEventMessage(event: RawEventData, color: Color?): Message {
        val builder = EmbedBuilder()
                .setTitle(event.name, event.website)
                .addField("Start", event.start, false)
                .addField("End", event.end, false)
                .setColor(color?.rgb ?: 0xEB459E)

        event.format?.let { builder.addField("Format", it, false) }
        event.access?.let { builder.addField("Access", "||$it||", false) }

        val embed = builder.build()

        return eventsChannel
                .sendMessageEmbeds(embed)
                .complete()
    }

    private fun createEventRole(event: RawEventData): Role {
        val colors = setOf(
            0x5865F2,
            0x57F287,
            0xFEE75C,
            0xEB459E,
            0xED4245,
        )

        return eventsChannel.guild.createRole()
                .setName(event.name)
                .setColor(colors.random())
                .setMentionable(true)
                .complete()
    }

    private fun createEventCategory(event: RawEventData, role: Role, message: Message): Category {
        return eventsChannel.guild.createCategory(event.name)
                .clearPermissionOverrides()
                .addPermissionOverride(role, setOf(Permission.VIEW_CHANNEL), emptySet())
                .setPosition(1)
                .complete()
    }

    private fun validate(data: RawEventData): Either<List<ValidationError>, RawEventData> {
        val checks = listOf(
            data.name.isBlank()               to ValidationError("Name must not be blank"),
            data.website.isBlank()            to ValidationError("Website must not be blank"),
            (safelyParseDate(data.start) == null) to ValidationError("Invalid start date"),
            (safelyParseDate(data.end) == null)   to ValidationError("Invalid end date"),
        )

        // If any of the specified predicates matches the validation is short-circuited
        if (checks.any { it.first }) {
            // Collect all validation errors to list
            return checks
                    .filter { it.first }
                    .map { it.second }
                    .left()
        }

        // Additional checks require a valid datetime
        val start = parse(data.start)
        val end = parse(data.end)

        val dateChecks = listOf(
            start.isAfter(end) to ValidationError("Start date must be before the end date"),
            start.isBefore(LocalDateTime.now()) to ValidationError("Start date must be after today")
        )

        if (dateChecks.any { it.first }) {
            return dateChecks
                    .filter { it.first }
                    .map { it.second }
                    .left()
        }

        return data.right()
    }

    private fun safelyParseDate(date: String): LocalDateTime? =
            try { parse(date) }
            catch (exception: Exception) { null }

    private fun safelyParseUUID(uuid: String): Option<UUID> =
            try { UUID.fromString(uuid).some() }
            catch (exception: Exception) { none() }

    private fun parse(date: String): LocalDateTime = LocalDateTime.parse(date, CzechDateTimeFormatter.default)
}