package dev.vrba.flaggie

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FlaggieApplication

fun main(args: Array<String>) {
    runApplication<FlaggieApplication>(*args)
}
