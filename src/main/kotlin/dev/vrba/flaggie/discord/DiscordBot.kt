package dev.vrba.flaggie.discord

import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.entities.Activity
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component

@Component
class DiscordBot(config: DiscordConfiguration) {

    private val client: JDA = JDABuilder.createDefault(config.token)
            .setActivity(Activity.playing("CTFs"))
            .build()
            .awaitReady()

    @Bean
    fun jda(): JDA = client
}