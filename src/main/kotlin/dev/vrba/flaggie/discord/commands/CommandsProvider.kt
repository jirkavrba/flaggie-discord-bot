package dev.vrba.flaggie.discord.commands

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.build.CommandData

interface CommandsProvider {
    val commands: List<CommandData>
    fun handle(event: SlashCommandEvent)
}