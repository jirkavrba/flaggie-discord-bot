package dev.vrba.flaggie.discord.commands

import dev.vrba.flaggie.entity.CtfEvent
import dev.vrba.flaggie.service.CtfEventsService
import dev.vrba.flaggie.service.CtfEventsService.RawEventData
import dev.vrba.flaggie.service.CtfEventsService.ValidationError
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.interactions.commands.OptionType
import net.dv8tion.jda.api.interactions.commands.build.CommandData
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData
import org.springframework.stereotype.Component
import java.util.*

@Component
class CtfEventsCommandsProvider(private val service: CtfEventsService) : CommandsProvider {

    override val commands: List<CommandData> = listOf(
            CommandData("event", "Manage CTF events")
                    .addSubcommands(
                            SubcommandData("create", "Creates a new CTF event + role + category")
                                    .addOption(OptionType.STRING, "name", "Name of the event (eg. Google CTF 2021)", true)
                                    .addOption(OptionType.STRING, "website", "Website of the event (eg. https://g.co/ctf)", true)
                                    .addOption(OptionType.STRING, "start", "Start of the event (eg. \"24. 12. 2021 18:00\")", true)
                                    .addOption(OptionType.STRING, "end", "End of the event (eg. \"25. 12. 2021 17:00\")", true)
                                    .addOption(OptionType.STRING, "format", "Format of the event (eg. Jeopardy)", false)
                                    .addOption(OptionType.STRING, "access", "Password/Team access token for registration", false),

                            SubcommandData("archive", "Deletes the specified event + roles, but preserves the associated channels")
                                    .addOption(OptionType.STRING, "id", "UUID of the event", true),

                            SubcommandData("delete", "Deletes the specified event + all associated channels")
                                    .addOption(OptionType.STRING, "id", "UUID of the event", true),
                    )
    )

    override fun handle(event: SlashCommandEvent) {
        when (event.subcommandName) {
            "create" -> handleCreate(event)
            "archive" -> handleArchive(event)
            "delete" -> handleDelete(event)
            else -> event.reply("Nope").queue()
        }
    }

    private fun handleCreate(event: SlashCommandEvent) {
        val data = RawEventData(
                requireNotNull(event.getOption("name")).asString,
                requireNotNull(event.getOption("website")).asString,
                requireNotNull(event.getOption("start")).asString,
                requireNotNull(event.getOption("end")).asString,
                event.getOption("format")?.asString,
                event.getOption("access")?.asString
        )

        service.create(data)
                .fold(
                        { reportErrors("Invalid argument(s) provided", it.map(ValidationError::reason), event) },
                        { confirmCreation(it, event) }
                )
    }

    private fun handleArchive(event: SlashCommandEvent) {
        service.archive(requireNotNull(event.getOption("id")).asString)
                .fold(
                        { reportErrors("Event not found. Available events:", it.available.map(UUID::toString), event) },
                        { confirmArchivation(it, event) }
                )
    }

    private fun handleDelete(event: SlashCommandEvent) {
        service.delete(requireNotNull(event.getOption("id")).asString)
                .fold(
                        { reportErrors("Event not found. Available events:", it.available.map(UUID::toString), event) },
                        { confirmDeletion(it, event) }
                )
    }

    private fun confirmCreation(entity: CtfEvent, event: SlashCommandEvent) =
    // Discord api needs to be called within the time frame of 3 seconds,
            // As there can be some delay with creating role / channels / message it is delayed
            event.deferReply()
                    .complete()
                    .sendMessageEmbeds(
                            EmbedBuilder()
                                    .setColor(0x57F287)
                                    .setTitle("CTF Event **${entity.name}** created")
                                    .addField("ID", entity.id.toString(), false)
                                    .addField("Role", "<@&${entity.roleId}>", false)
                                    .addField("Message ID", entity.messageId, false)
                                    .addField("Category ID", entity.categoryId, false)
                                    .build()
                    )
                    .queue()

    private fun confirmArchivation(entity: CtfEvent, event: SlashCommandEvent) =
            event.deferReply()
                    .complete()
                    .sendMessageEmbeds(
                            EmbedBuilder()
                                    .setColor(0x57F287)
                                    .setTitle("CTF Event **${entity.name}** archived")
                                    .build()
                    )
                    .queue()

    private fun confirmDeletion(entity: CtfEvent, event: SlashCommandEvent) =
            event.deferReply()
                    .complete()
                    .sendMessageEmbeds(
                            EmbedBuilder()
                                    .setColor(0x57F287)
                                    .setTitle("CTF Event **${entity.name}** deleted")
                                    .build()
                    )
                    .queue()

    private fun reportErrors(title: String, errors: List<String>, event: SlashCommandEvent) =
            event.replyEmbeds(
                    EmbedBuilder()
                            .setColor(0xED4245)
                            .setTitle(title)
                            .setDescription(errors.joinToString("\n"))
                            .build())
                    .queue()
}