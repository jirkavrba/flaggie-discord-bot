package dev.vrba.flaggie.discord.listeners

import arrow.core.Option
import dev.vrba.flaggie.discord.DiscordConfiguration
import dev.vrba.flaggie.repository.CtfEventsRepository
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Role
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import net.dv8tion.jda.api.requests.RestAction
import org.springframework.stereotype.Component

@Component
final class CtfEventsReactionsListener(
        private val repository: CtfEventsRepository,
        private val config: DiscordConfiguration,
        client: JDA
) : ListenerAdapter() {

    init {
        client.addEventListener(this)
    }

    override fun onMessageReactionAdd(event: MessageReactionAddEvent) =
        processEvent(event) {
            guild, member, role -> guild.addRoleToMember(member, role)
        }

    override fun onMessageReactionRemove(event: MessageReactionRemoveEvent) =
        processEvent(event) {
            guild, member, role -> guild.removeRoleFromMember(member, role)
        }

    private fun processEvent(event: GenericMessageReactionEvent, handler: (Guild, Member, Role) -> RestAction<*>) {
        if (!shouldHandle(event)) return

        val entity = repository.findByMessageId(event.messageId) ?: return
        val member = Option.fromNullable(event.member)
        val role = Option.fromNullable(event.guild.getRoleById(entity.roleId))

        member.zip(role).map {
            handler(event.guild, it.first, it.second).queue()
        }
    }

    private fun shouldHandle(event: GenericMessageReactionEvent): Boolean =
            isNotFromBot(event) &&
            event.isFromGuild &&
            event.guild.id == config.guild &&
            event.channel.id == config.eventsChannel &&
            event.reaction.reactionEmote.emoji == config.eventsReaction

    private fun isNotFromBot(event: GenericMessageReactionEvent): Boolean =
            !(event.user?.isBot ?: true)
}